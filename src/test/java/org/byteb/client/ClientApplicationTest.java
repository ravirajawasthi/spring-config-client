package org.byteb.client;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ClientApplicationTest {
    @Value("${backend-url}")
    String name;

    @Test
    void contextLoads() {
        assertEquals(name, "thor-test");
    }

}
