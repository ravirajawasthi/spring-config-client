package org.byteb.client;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @Value("${backend-url}")
    String val;

    @Value("${image}")
    private Resource image;

    @Value("${common-property-for-all}")
    private String common;


    @GetMapping("/")
    public String returnUrl() {
        return val;
    }
}